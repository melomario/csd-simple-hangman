const files = require("fs");

const WORD_FILE = "./assets/words.txt";

const getRandomWord = () => {
  let words_array = readWordsFile(WORD_FILE);
  return getRandomWordFromList(words_array);
};

const getRandomWordFromList= words_array => {
  // Como obter um inteiro entre 0 e o tamanho máximo de palavras (words_array.length)?
  let indice_aleatorio = generateRandomInteger(0, words_array.length)
  return words_array[indice_aleatorio];
};

const generateRandomInteger = (min, max) => {
  return Math.floor(min + Math.random()*(max - min))
}

const readWordsFile = filename => {
  const words_array = files.readFileSync(filename, "utf-8").split("\n");
  return words_array;
};

module.exports = {
  WORD_FILE,
  readWordsFile,
  getRandomWordFromList,
  getRandomWord
};