const GameEngine = require("./engine");
const readline = require("readline");

const linereader = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const run = () => {
  const game = GameEngine.startGame()

  console.log("**********************");
  console.log("*     NOVO  JOGO     *");
  console.log("**********************");
  game_loop(game);
};

const game_loop = game => {
  console.log(game.status)
  if(game.status === "RUNNING"){
    printGameState(game);
    evaluateGuess(game);
  }
  else if (game.status === "REPEATED") {
    console.log("Esta letra já foi utilizada :)");
    evaluateGuess(game);
  }
  else if (game.status === "LOST" ) {
    console.log("PERDEU :< Não tem mais tentativas");
    console.log("A palavra era: " + game.word)
  }
  else if (game.status === "WIN" ) {
    console.log("GANHOU :)");
    console.log("A palavra era: " + game.word)
  }
};

const printGameState = game => {
  console.log(getForca(game.lives))
  console.log("Remaining Lives: " + game.lives);
  console.log( game.display_word);
  console.log( game.guesses );
};

const evaluateGuess = game => {
  linereader.question("Guess a letter: ", letter => {
    updated_game = GameEngine.takeGuess(game, letter);
    game_loop(updated_game);
  });
};

const getForca = (lives) => {
  if(lives == 6){
    return "|----  \n" +
    "|   |  \n" + 
    "|      \n" +
    "|      \n" +
    "|      \n" +
    "|      \n" +
    "--------"
  }
  else{
    return "|----  \n" +
    "|   |  \n" + 
    "|   😤  \n" +
    "|      \n" +
    "|      \n" +
    "|      \n" +
    "--------"
  }
}

module.exports = {
  run
};
