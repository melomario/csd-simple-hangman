const dictionary = require("./dictionary")

const startGame = () => {
  
  const word = dictionary.getRandomWord()
  
  return {
    status: "RUNNING",
    word: word,
    lives: 6,
    display_word: getDisplayWord(word, []),
    guesses: []
  };
};

const takeGuess = (game_state, guess) => {

  //Definir o Estado para RUNNING
  game_state.status = "RUNNING"

  //adicionar letra na lista de tentativas
  if(game_state.guesses.includes(guess)){
    game_state.status = "REPEATED"
  }
  else{
    game_state.guesses.push(guess)
  }

  game_state.display_word = getDisplayWord(game_state.word, game_state.guesses)

  //estamos validando se a letra existe na palvra
  if (!game_state.word.includes(guess)) {
    game_state.lives -= 1
  } 
  
  //caso já não exista espaços em branco, finalisa o jogo
  if (!game_state.display_word.includes("_")) {
    game_state.status = "WIN"      // O bug é porque seria game_state.status
  }
  
  //caso não existam tentativas muda o estado para o estado LOST
  if (game_state.lives == 0) {
    game_state.status = "LOST"     // O bug é porque seria game_state.status :)
  }

  return game_state

};

const getDisplayWord = (palavra, palpites) => {
  return palavra.split("")
  .map(letra => palpites.includes(letra) ? letra : "_")
  .join(" ")
}

module.exports = {
  startGame,
  takeGuess
};
