const Dictionary = require("../lib/dictionary");

describe("Nossa biblioteca de palavras aleatorias deve ", () => {
  test("ter um volume considerável de palavras", () => {
    expect( Dictionary.readWordsFile(Dictionary.WORD_FILE) ).toHaveLength(10);
  });

  test("ter palavras diferentes, com tamanhos diferentes", () => {
    let lista_palavras = Dictionary.readWordsFile(Dictionary.WORD_FILE)
    tamanhos_das_palavras = lista_palavras.map( palavra =>  palavra.length )
    
    tamanhos_das_palavras.forEach(tamanho => 
      expect(tamanho).toBeGreaterThan(5)  
    )

  });

});


