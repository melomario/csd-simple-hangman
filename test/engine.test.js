const GameEngine = require("../lib/engine");

describe("O jogo da forca deve ", () => {
  test("começar com um estado válido", () => {
    let state = GameEngine.startGame()

    expect( state.lives ).toBe( 6 )
    expect( state.status ).toBe( "RUNNING" )
    expect( state.word.length ).toBeGreaterThan( 4 )
    expect( state.guesses.length ).toBe( 0 )
    expect( state.display_word.length ).toBe( (state.word.length * 2) - 1)
  });

  // Grupo 1
  test("atualizar a palavra exibida se a letra fizer parte da palavra", () => {
    let estado_de_teste = {
      status: "RUNNING",
      word: "garrafa",
      lives: 2,
      display_word: "g _ _ _ _ _ _",
      guesses: ["x", "y", "m", "s", "g"]
    }

    const palpites_antes_do_teste = estado_de_teste.guesses.length

    let novo_estado = GameEngine.takeGuess(estado_de_teste, "f")

    expect( novo_estado.lives ).toBe( estado_de_teste.lives )
    expect( novo_estado.status ).toBe( "RUNNING" )
    expect( novo_estado.guesses.length ).toBe( palpites_antes_do_teste + 1 )
    expect( novo_estado.display_word ).toBe( "g _ _ _ _ f _" )
    expect( novo_estado.guesses ).toContain( "f" )

  });

  // Grupo 2
  test("atualizar a palavra exibida quando a letra aparece em mais de um sítio", () => {
    let estado_de_teste = {
      status: "RUNNING",
      word: "papel",
      lives: 4,
      display_word: "_ _ _ _ _",
      guesses: ["x", "y"]
    }

    let novo_estado = GameEngine.takeGuess(estado_de_teste, "p")

    //o número de vidas não muda
    expect( novo_estado.lives ).toBe( estado_de_teste.lives )
    //o estado é RUNNING
    expect( novo_estado.status ).toBe( "RUNNING" )
    //a palavra exibida deve ser "p _ p _ _"
    expect( novo_estado.display_word ).toBe( "p _ p _ _" )
    //incluir essa letra na lista de letras usadas
    expect( novo_estado.guesses ).toEqual( ["x", "y", "p"] )

  });

  // Grupo 1
  test("finalizar o jogo com sucesso caso todas as letras tenham sido adivinhadas", () => {
    let estado_de_teste = {
      status: "RUNNING",
      word: "garrafa",
      lives: 2,
      display_word: "g a r r a _ a",
      guesses: ["x", "y", "m", "s", "g", "a", "r"]
    }

    const palpites_antes_do_teste = estado_de_teste.guesses.length
    let novo_estado = GameEngine.takeGuess(estado_de_teste, "f")

    expect( novo_estado.lives ).toBe( estado_de_teste.lives )
    expect( novo_estado.status ).toBe( "WIN" )
    expect( novo_estado.guesses.length ).toBe( palpites_antes_do_teste + 1 )
    expect( novo_estado.display_word ).toBe( "g a r r a f a" )
    
  });

  // Grupo 2
  test("diminuir o número de tentativas e armazenar a tentativa caso o palpite seja errado", () => {
    let estado_de_teste = {
      status: "RUNNING",
      word: "papel",
      lives: 4,
      display_word: "_ _ _ _ _",
      guesses: ["x", "y"]
    }

    let novo_estado = GameEngine.takeGuess(estado_de_teste, "t")

    //retira uma vida
    expect( novo_estado.lives ).toBe( 3 )
    //o estado é RUNNING
    expect( novo_estado.status ).toBe( "RUNNING" )
    //a palavra exibida deve ser "_ _ _ _ _"
    expect( novo_estado.display_word ).toBe( "_ _ _ _ _" )
    //incluir essa letra na lista de letras usadas
    expect( novo_estado.guesses ).toContain("t")
  });

  // Grupo 1
  test("finalizar o jogo caso o número de tentativas se esgote", () => {
    let estado_de_teste = {
      status: "RUNNING",
      word: "garrafa",
      lives: 1,
      display_word: "g a r r a _ a",
      guesses: ["x", "y", "m", "s", "g", "a", "r"]
    }

    const palpites_antigos = estado_de_teste.guesses.length

    let novo_estado = GameEngine.takeGuess(estado_de_teste, "t")

    expect( novo_estado.lives ).toBe( 0 )
    expect( novo_estado.status ).toBe( "LOST" )
    expect( novo_estado.guesses.length ).toBe( palpites_antigos + 1 )
  });

  // Grupo 2
  test("não fazer nada quando há uma tentativa repetida", () => {
    let estado_de_teste = {
      status: "RUNNING",
      word: "papel",
      lives: 4,
      display_word: "p _ p _ _",
      guesses: ["x", "y", "p"]
    }

    let novo_estado = GameEngine.takeGuess(estado_de_teste, "p")

    //o número de vidas não muda
    expect( novo_estado.lives ).toBe( estado_de_teste.lives )
    //o estado tem de ser REPEATED
    expect( novo_estado.status ).toBe( "REPEATED" )
    //a palavra exibida deve ser "p _ p _ _"
    expect( novo_estado.display_word ).toBe( "p _ p _ _" )
    //as tentativas ficam iguais
    expect( novo_estado.guesses ).toBe( estado_de_teste.guesses )
  });

});


